import React, { Component } from 'react';
import Counter from './counter';

class Counters extends React.Component {
    state = {
        allItems: [
            { id:0, value:20 , name:"yes0"},
            { id:1, value:1  , name:"yes1"},
            { id:2, value:0  , name:"yes2"},
            { id:3, value:8  , name:"yes3"}
        ]
    };

    deleteItem = itemId => {
        const allItems = this.state.allItems.filter( i => i.id !== itemId);
        this.setState({allItems});
    };

    render() { 
        return (
        <div>
            {this.state.allItems.map(counter => (
                <Counter 
                    key={counter.id} 
                    value={counter.value} 
                    onDelete={() => this.deleteItem(counter.id)}>
                    <h4>{counter.name}</h4>
                </Counter>
            ))}
        </div>
        );
    };
}
 
export default Counters;