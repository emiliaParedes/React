import React, { Component } from 'react';

class Counter extends React.Component {
    //state, private variables 
    //props read only from info given
    state = {
        value: this.props.value,
        imageUrl: 'https:picsum.photos/200',
        listElements: ["tag0", "tag1", "tag2"]
    };
    
    styles = {
        fontSize: 50,
        fontWeight: "bold"
    }

    //constructor(){
        //parent constructor
        //super();
        //link the functions to this object no longer necesary, can use functionname = () =>
        //this.incrementCounter = this.incrementCounter.bind(this);
    //}

    render() {
        //editing the class on runtime
        let thisClass = "badge m-2 bg-";
        thisClass += this.state.value === 0 ? "warning" : "primary";

        //React.Fragment is for everything (h1 and button) are not wrapped by
        //a div (which will be necessary if react.fragment is not used)
        //it is cleaner to using div (in html form)
        return (
            <React.Fragment>
                {this.props.children}
                <span style={this.styles} class={thisClass}> 
                    {this.formatCount()}
                </span>
                <img src={this.state.imageUrl} alt="" />
                <button 
                    onClick={ () => this.incrementCounter() } 
                    class="btn btn-secondary btn-sm"
                >
                    ++
                </button>
                {this.renderListElements()}
                <button 
                    onClick={this.props.onDelete} 
                    class="btn btn-danger btn-sm m-2"> 
                    Delete Item
                </button>
            </React.Fragment>
        );
    }

    incrementCounter = () => {
        this.state.value++;
        //update the screen 
        this.setState({value: this.state.value});
        console.log(this.props);
    }

    renderListElements(){
        if(this.state.listElements.length === 0 )
            return <p>There are no elements in the list</p>;

        //in the <ul> the key is currently just listElements name tag but they could be objects 
        //and each can have an id so we can do key={tag.id}
        return <ul>{this.state.listElements.map(tag => <li key={tag}>{tag}</li>)}</ul>;
    }

    formatCount() {
        const {value} = this.state;
        return value === 0 ? "Zero" : value;
    }
}
 
export default Counter;
